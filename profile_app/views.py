from django.shortcuts import render

# Create your views here.
response = {}
def profile(request):
	response['profile_name'] = 'Kevin Christian Chandra'
	response['motto'] = '\"Do your best in everything you do.\"'
	return render(request, 'profile.html', response)