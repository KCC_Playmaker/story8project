from django.conf.urls import url
from django.urls import path
from .views import books, get_json

urlpatterns = [
    path('', books, name='books'),
    path('/get_books', get_json, name='get_books'),
]