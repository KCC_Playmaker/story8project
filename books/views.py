from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
response = {}
def books(request):
    response['page_title'] = 'My Books'
    response['head'] = 'List of My Collection'
    response['motto'] = '\"Do your best in everything you do.\"'
    return render(request, 'books.html', response)

def get_json(request):
    response = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting")
    data = response.json() # dict
    return JsonResponse(data)